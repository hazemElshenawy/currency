import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MyDataService} from "./my-data.service";
import {AuthenticationService} from "./authentication.service";
import { HttpClientModule , HttpClient} from '@angular/common/http';
import { HttpModule  } from '@angular/http';
import { CurrencyComponent } from './currency/currency.component';
import {APP_BASE_HREF, HashLocationStrategy, LocationStrategy} from '@angular/common';


const appRoutes : Routes =[

  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'login',
    component:LoginComponent
  },
  {
    path:'register',
    component:RegisterComponent
  },
  {
    path:'currency',
    component:CurrencyComponent
  }

];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CurrencyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,HttpModule,
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  providers: [AuthenticationService,MyDataService],
  bootstrap: [AppComponent]
})
export class AppModule {



 }
