import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from './../authentication.service';
import {Router} from '@angular/router';
import {MyDataService} from './../my-data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  messageError: string = "";
  messageCountryError: string = "";
  messageEmailError: string = "";
  messageNameError: string = "";
  messagePasswordError: string = "";
  messageConfirmPasswordError: string = "";
  messageUserNameError: string = "";
  fullImagePath: string;
  resNull: string = "";
  res: string = "";

  display: string = "";
  Username: string = "";
  password: string = "";
  confirmPassword: string = "";
  Name: string = "";
  Email: string = "";
  selectedCountry: any;
  country: any;
  defaultCountry: any;
  passLength=0;





  selectedPrototypeSelector;
  prototypes = [{id: 'selectedcountry........'}, {id: 'Afghanistan'}, {id: 'Albania'}
  , {id: 'Algeria'}, {id: 'Andorra'}, {id: 'Angola'}
  , {id: 'Antigua and Barbuda'}, {id: 'Argentina'}, {id: 'Armenia'}
  , {id: 'Australia'}, {id: 'Austria'}, {id: 'Azerbaijan'}
  , {id: 'Bahamas'}, {id: 'Bahrain'}, {id: 'Bangladesh'}
  , {id: 'Barbados'}, {id: 'Belarus'}, {id: 'Belgium'}
  , {id: 'Belize'}, {id: 'Benin'}, {id: 'Bhutan'}
  , {id: 'Bolivia'}, {id: 'Bosnia and Herzegovina'}, {id: 'Botswana'}
  , {id: 'Brazil'}, {id: 'Brunei'}, {id: 'Bulgaria'}
  , {id: 'Burkina Faso'}, {id: 'Burundi'}, {id: 'Cabo Verde'}
  , {id: 'Cambodia'}, {id: 'Cameroon'}, {id: 'Canada'}
  , {id: 'Central African Republic (CAR)'}, {id: 'Chad'}, {id: 'Chile'}
  , {id: 'China'}, {id: 'Colombia'}, {id: 'Comoros'}
  , {id: 'Democratic Republic of the Congo'}, {id: 'Republic of the Congo'}, {id: 'Costa Rica'}
  , {id: 'Cote dIvoire'}, {id: 'Croatia'}, {id: 'Cuba'}
  , {id: 'Cyprus'}, {id: 'Czech Republic'}, {id: 'Denmark'}
  , {id: 'Djibouti'}, {id: 'Dominica'}, {id: 'Dominican Republic'}
  , {id: 'Ecuador'}, {id: 'El Salvador'}, {id: 'Egypt'}
  , {id: 'Equatorial Guinea'}, {id: 'Eritrea'}, {id: 'Estonia'}
  , {id: 'Ethiopia'}, {id: 'Fiji'}, {id: 'Finland'}
  , {id: 'France'}, {id: 'Gabon'}, {id: 'Gambia'}
  , {id: 'Georgia'}, {id: 'Germany'}, {id: 'Ghana'}
  , {id: 'Greece'}, {id: 'Grenada'}, {id: 'Guatemala'}
  , {id: 'Guinea'}, {id: 'Guinea-Bissau'}, {id: 'Guyana'}
  , {id: 'Haiti'}, {id: 'Honduras'}, {id: 'Hungary'}
  , {id: 'Iceland'}, {id: 'India'}, {id: 'Indonesia'}
  , {id: 'Iran'}, {id: 'Iraq'}, {id: 'Ireland'}
  , {id: 'Israel'}, {id: 'Italy'}, {id: 'Jamaica'}
  , {id: 'Japan'}, {id: 'Jordan'}, {id: 'Kazakhstan'}
  , {id: 'Kenya'}, {id: 'Kiribati'}, {id: 'Kosovo'}
  , {id: 'Kuwait'}, {id: 'Kyrgyzstan'}, {id: 'Laos'}
  , {id: 'Latvia'}, {id: 'Lebanon'}, {id: 'Lesotho'}
  , {id: 'Liberia'}, {id: 'Libya'}, {id: 'Liechtenstein'}
  , {id: 'Lithuania'}, {id: 'Luxembourg'}, {id: 'Macedonia (FYROM)'}
  , {id: 'Madagascar'}, {id: 'Malawi'}, {id: 'Malaysia'}
  , {id: 'Maldives'}, {id: 'Mali'}, {id: 'Malta'}
  , {id: 'Marshall Islands'}, {id: 'Mauritania'}, {id: 'Mauritius'}
  , {id: 'Mexico'}, {id: 'Micronesia'}, {id: 'Moldova'}
  , {id: 'Monaco'}, {id: 'Mongolia'}, {id: 'Montenegro'}
  , {id: 'Morocco'}, {id: 'Mozambique'}, {id: 'Myanmar (Burma)'}
  , {id: 'Namibia'}, {id: 'Nauru'}, {id: 'Nepal'}
  , {id: 'Netherlands'}, {id: 'New Zealand'}, {id: 'Nicaragua'}
  , {id: 'Niger'}, {id: 'Nigeria'}, {id: 'North Korea'}
  , {id: 'Norway'}, {id: 'Oman'}, {id: 'Pakistan'}
  , {id: 'Palau'}, {id: 'Palestine'}, {id: 'Panama'}
  , {id: 'Papua New Guinea'}, {id: 'Paraguay'}, {id: 'Peru'}
  , {id: 'Philippines'}, {id: 'Poland'}, {id: 'Portugal'}
  , {id: 'Qatar'}, {id: 'Romania'}, {id: 'Russia'}
  , {id: 'Rwanda'}, {id: 'Saint Kitts and Nevis'}, {id: 'Saint Lucia'}
  , {id: 'Saint Vincent and the Grenadines'}, {id: 'Samoa'}, {id: 'San Marino'}
  , {id: 'Sao Tome and Principe'}, {id: 'Saudi Arabia'}, {id: 'Senegal'}
  , {id: 'Serbia'}, {id: 'Seychelles'}, {id: 'Sierra Leone'}
  , {id: 'Singapore'}, {id: 'Slovakia'}, {id: 'Slovenia'}
  , {id: 'Solomon Islands'}, {id: 'Somalia'}, {id: 'South Africa'}
  , {id: 'South Korea'}, {id: 'South Sudan'}, {id: 'Spain'}
  , {id: 'Sri Lanka'}, {id: 'Sudan'}, {id: 'Suriname'}
  , {id: 'Swaziland'}, {id: 'Sweden'}, {id: 'Switzerland'}
  , {id: 'Syria'}, {id: 'Taiwan'}, {id: 'Tajikistan'}
  , {id: 'Tanzania'}, {id: 'Thailand'}, {id: 'Timor-Leste'}
  , {id: 'Togo'}, {id: 'Tonga'}, {id: 'Trinidad and Tobago'}
  , {id: 'Tunisia'}, {id: 'Turkey'}, {id: 'Turkmenistan'}
  , {id: 'Tuvalu'}, {id: 'Uganda'}, {id: 'Ukraine'}
  , {id: 'United Arab Emirates (UAE)'}, {id: 'United Kingdom (UK)'}, {id: 'United States of America (USA)'}
  , {id: 'Uruguay'}, {id: 'Uzbekistan'}, {id: 'Vanuatu'}
  , {id: 'Vatican City (Holy See)'}, {id: 'Venezuela'}, {id: 'Vietnam'}
  , {id: 'Yemen'}, {id: 'Zambia'}, {id: 'Zimbabwe'}

];


  constructor( private router: Router, private authenticationService: AuthenticationService, private newSerive: MyDataService) {

   }

  ngOnInit() {
  }

  onChange(value: any) {
    //alert(value);

    if (this.confirmPassword != this.password) {
      this.messageConfirmPasswordError = " *Password mismatch";
    }
    else if (this.confirmPassword == this.password) {
      this.messageConfirmPasswordError = "";
    }

    if (this.confirmPassword == "") {
      this.messageConfirmPasswordError = "";
    }

  }

  onPrototypeChange() {
    this.country = this.selectedPrototypeSelector;
    console.debug(this.selectedPrototypeSelector);
  }

  passwordlength(value) {
    // var x= $("passwordInput").val ;
    //  alert(value);
    this.passLength=value.length;
    // if(this.passLength)
  }

  RegisterUser(e) {

    e.preventDefault();
    //console.log(e);
    var email = e.target.elements[0].value;
    var Name = e.target.elements[1].value;
    var UserName = e.target.elements[2].value;
    var Password = e.target.elements[3].value;
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (UserName == "") {
      this.messageUserNameError = "* please Enter UserName ";
    }
    else if (UserName != "") {
      this.messageUserNameError = "";
    }
    if (Name == "") {
      this.messageNameError = "* please Enter Name ";
    }
    else if (Name != "") {
      this.messageNameError = "";
    }
    if (Password == "") {
      this.messagePasswordError = "* please Enter Password ";
    }
    else if (Password != "") {
      this.messagePasswordError = "";
    }
    if (this.confirmPassword == "" || (this.confirmPassword != this.password)) {
      this.messageConfirmPasswordError = " *Password mismatch";
    }
    else {
      this.messageConfirmPasswordError = "";
    }
    if (!EMAIL_REGEXP.test(email)) {
      this.messageEmailError = "* please Enter valid Email";
      return {invalidEmail: true};
    } else {
      this.messageEmailError = "";
    }
    if (this.country != this.defaultCountry) {
      this.messageCountryError = "";

    }
    else {

      this.messageCountryError = "* please choose Country";
    }

    if (UserName != "" && Name != "" && Password != "" && this.confirmPassword != "" && email != this.resNull && this.country != this.defaultCountry) {
      var resName = Name.split(" ");
      var resUserName = UserName.split(" ");
      this.newSerive.registerUser(resUserName, resName, email, Password, this.country.id, this.confirmPassword).subscribe(data => {
          if (data == "true") {
            this.res = data;
            console.log(" >>>> navigate to login <<<<");
            this.router.navigate(['login']);
            //this.display="SuccessFul Registeration";
          } else {
            console.log(" >>>> navigate to register-form <<<<");
            this.router.navigate(['register-form']);
            this.display = "this user is exist before ";
          }
        }
      );
    }
    else {
      console.log(" >>>> 2- navigate to register-form (2) <<<<");
      this.router.navigate(['register-form']);

    }

  }

}
