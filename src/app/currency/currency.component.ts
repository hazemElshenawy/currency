import { AuthenticationService } from './../authentication.service';
import { Component, OnInit } from '@angular/core';
import {Router ,NavigationEnd}from '@angular/router';
import { MyDataService } from './../my-data.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.css']
})
export class CurrencyComponent implements OnInit {

  model: any = {};
  sign;
  selectOption: any = [{id: '1'}, {id: '2'}, {id: '3'}];
  selectedPrototypeSelector;

  itemsList :any =[{name:'Before Amount',value:'1'}, {name:'After Amount',value:'2'}]  ;
  BeforeAfterAmount:any;
  radioSelected:string;
  radioSelectedString:string;
  isChecked;
  dataObject:any=[];
  temp_token;
  files;

  constructor(private authenticationService: AuthenticationService,private router :Router,private newSerive: MyDataService) { }

  ngOnInit() {
    this.temp_token = this.authenticationService.getStringToken();
    this.getData();
  }

  logOut(){
  this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  save(){

    this.dataObject.push({
      'CurrencyID':this.model.CurrencyID,
      'Description':this.model.Description,
      'sign':this.sign,
      'CurrencySymbol':this.model.CurrencySymbol,
      'ArabicDescription':this.model.ArabicDescription,
      'BeforeAfterAmount':this.BeforeAfterAmount.value,
      'decimal':this.model.decimal,
      'thousands':this.model.thousands,
      'isChecked':this.isChecked,
      'CurrencyUnit':this.model.CurrencyUnit,
      'SubunitConnector':this.model.SubunitConnector,
      'Currencysubunit':this.model.Currencysubunit,
      'CurrencyUnitArabic':this.model.CurrencyUnitArabic,
      'SubunitConnectorArabic':this.model.SubunitConnectorArabic,
      'CurrencysubunitArabic':this.model.CurrencysubunitArabic,
      'token':this.temp_token
    });

    this.newSerive.saveCurrency(JSON.stringify(this.dataObject)).subscribe(data => {
      if (data == "true") {
        this.getData();

      } else {

      }
    }
  );
  }

  onPrototypeChange() {
    this.sign = this.selectedPrototypeSelector.id;
    //alert( this.sign);
  }

   // Get row item from array
   getSelecteditem(){
    this.BeforeAfterAmount = this.itemsList.find(Item => Item.value === this.radioSelected);
    this.radioSelectedString = JSON.stringify(this.BeforeAfterAmount);
  }
  // Radio Change Event
  onItemChange(item){
    this.getSelecteditem();
  }
  checkValue(event: any){
    console.log(event);
    this.isChecked = event;
 }

getData()
{
  this.newSerive.getCurrency(this.temp_token).subscribe(result => {
    this.files = result;
  });
}

}
