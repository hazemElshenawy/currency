import { Component, OnInit } from '@angular/core';
import {MyDataService} from "../my-data.service";
import {NavigationEnd, Router} from "@angular/router";
import {AuthenticationService} from "../authentication.service";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  error = '';
  constructor(private router: Router,private authenticationService: AuthenticationService,private newSerive: MyDataService) {

  }

  ngOnInit() {
  }

  login() {

    this.authenticationService.login(this.model.username, this.model.password)
    .subscribe(result => {
      if (result === true)
      {
        var token = this.authenticationService.getToken();
        this.router.navigate(['home']);
      } else {
        this.error = 'Username or password is incorrect';
      }
    }, error => {
      this.error = error;
    });

  }

  register(){
    this.router.navigate(['register']);
  }

}
