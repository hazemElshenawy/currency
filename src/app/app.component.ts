import { Component } from '@angular/core';
import {Router ,NavigationEnd}from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {MyDataService} from './my-data.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'currency';


  constructor(private router :Router,private authenticationService: AuthenticationService)
 {
  var uN = this.authenticationService.getTokenUN();
  if(!uN){
    this.router.navigate(['login']);
  }

 }

 ngOnInit(){

 }

}
