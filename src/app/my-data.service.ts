import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import {Http, Response, Headers, RequestOptions} from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class MyDataService {

  ip:any='http://192.168.1.207';
  url:any=this.ip+':8080';
  constructor(private http: Http, public authenticationService: AuthenticationService) { }

  registerUser(FristName: string, lastName: string, email: string, Password: string, country: any, confirm_password: any) {
    var body = FristName + " " + lastName + " " + email + " " + Password + " " + country + " " + confirm_password;
    return this.http.post(this.url + '/RegisterUser', body).map((res: Response) => res.text());
  }

  saveCurrency(body) {
    var body = body;
    return this.http.post(this.url + '/saveCurrency', body).map((res: Response) => res.text());
  }

  getCurrency(token: string) {
    return this.http.post(this.url + `/getCurrency`, token)
      .map((res: Response) => res.json());
  }

}
