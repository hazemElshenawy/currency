import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private authUrl = 'http://192.168.1.207:8080/checkexsitiance';

  private headers = new Headers({'Content-Type': 'application/x-some-custom-type'});

  constructor(private http: Http) {
  }


  login(username: string, password: string): Observable<boolean> {
    return this.http.post(this.authUrl, JSON.stringify({
      username: username,
      password: password
    }), {headers: this.headers})
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let token = response.text();
        if (token) {

          localStorage.setItem('currentUser', JSON.stringify({username: username, token: token}));
          localStorage.setItem('username', JSON.stringify(username));
          localStorage.setItem('token', JSON.stringify(token));
          localStorage.setItem('usernamePassword', JSON.stringify(username));

          return true;
        } else {

          return false;
        }
      }).catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  getToken(): String {
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    var token = currentUser && currentUser.token;
    return token ? token : "";
  }

  getStringToken(): string {
    var currentUser = JSON.parse(localStorage.getItem('token'));
    return currentUser;
  }

  getTokenUP(): string {
    var currentUser = JSON.parse(localStorage.getItem('usernamePassword'));
    return currentUser;
  }

  getTokenUN(): String {
    var currentUser = JSON.parse(localStorage.getItem('username'));
    console.log("currentUser")
    console.log(currentUser)
    return currentUser;
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('username');
  }


}
